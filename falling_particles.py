#Shows 3 graphics with the data of 3 falling particles
#Allows the user to put the data via consola
from class_falling_ball import FallingBall
import matplotlib.pyplot as plt

#Create and show the plot
def plot_(times, y_s, num):
	for cont in range(0,num):
		plt.plot(times[cont],y_s[cont], '.')
	plt.xlabel("Time(s)")
	plt.ylabel("Position")
	plt.show()

#Fills the parameters of the object
def parameters_falling_ball(fb, y0, v0):
	fb.t = 0
	fb.dt = 0.01
	fb.y = y0
	fb.v = v0

#Fills the vectors with the data to create the plot
def calculate_positions(fall_ball, vel, time, y, y0, v0):
	time.append(0)
	vel.append(0)
	y.append(y0)
	while fall_ball.y > 0:
		fall_ball.step()
		vel.append(fall_ball.v)
		time.append(fall_ball.t)
		y.append(fall_ball.y)
	print("Results")
	print("Final time", fall_ball.t)
	print("y =", fall_ball.y, ", v =", fall_ball.v)
	print("Analitical position", fall_ball.analyticPosition(y0, v0))
	print("Analitical velocity", fall_ball.analyticVelocity(v0))
	print("Gravity", fall_ball.g)

#Dinamically object creation of falling_balls
def fall_ball_creator(num_objects):
	fall_balls = []								#Objects vector
	times = []									#Vector with the times to make the plot
	y_s = []									#Vector with the positions to make the plot
	for cont in range(0, num_objects):
		fall_balls.append(FallingBall())
	
	print("Insert the data")
	
	for cont in range(0, num_objects):
		print("---Object ", cont+1, "---")
		y0 = int(input("Value of y0: "))
		v0 = int(input("Value of v0: "))
		parameters_falling_ball(fall_balls[cont], y0, v0)
		vel = []
		time = []
		y = []
		calculate_positions(fall_balls[cont], vel, time, y, y0, v0)
		times.append(time)
		y_s.append(y)

	plot_(times, y_s, num_objects)

#Main
num_object = int(input("Insert the number of falling particles to plot: "))
fall_ball_creator(num_object)



