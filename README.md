# README #

This program is a simple plot generator that allows the user to show the movement of a particle falling.

The code have a class that recreates the behavior and the elements of a ball/particle, and the main class uses this object to reproduce the movement of falling. 

The user can select the number of the particles and the position and velocity variables of each ball.

Finally a the result is shown with all the data of the particles in the same plot.


The image below shows the data of 5 different particles:
![Captura de pantalla 2017-07-12 a las 12.22.56.png](https://bitbucket.org/repo/LoXa5k8/images/2938428429-Captura%20de%20pantalla%202017-07-12%20a%20las%2012.22.56.png)